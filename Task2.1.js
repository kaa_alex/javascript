'use strict';

function nameEnter(rl) {
    rl.question(`Введите ваше имя `, function (answer) {
        if (!answer) {
            console.log('Ввод не может быть пустым.');
            nameEnter(rl);
        }
        else {
            data["Имя"] = answer;
            middleNameEnter(rl);
        }
    });
}

function middleNameEnter(rl) {
    rl.question(`Введите ваше отчество `, function (answer) {
        if (!answer) {
            console.log('Ввод не может быть пустым.');
            middleNameEnter(rl);
        }
        else {
            data["Отчество"] = answer;
            lastNameEnter(rl);
        }
    });
}

function lastNameEnter(rl) {
    rl.question(`Введите вашу фамилию `, function (answer) {
        if (!answer) {
            console.log('Ввод  не может быть пустым.');
            lastNameEnter(rl);
        }
        else {
            data["Фамилия"] = answer;
            ageEnter(rl);
        }
    });
}

function ageEnter(rl) {
    rl.question(`Введите ваш возраст `, function (answer) {
        if (!answer) {
            console.log('Ввод не может быть пустым.');
            ageEnter(rl);
        }
        else {
            let age = Number(answer);
            if (age != NaN && age > 0) {
                data["Возраст"] = age;
                data["Возраст5"] = age + 5;
                data["Дни"] = age * 365;
                missionComplete(rl);
            } else {
                console.log('Ввод некорректен, попробуйте еще раз ')
                ageEnter(rl);
            }
        }
    });
}

function missionComplete(rl) {
    console.log(`ФИО: ${data.Фамилия} ${data.Имя} ${data.Отчество}, вам ${data.Возраст} лет, вам ${data.Дни} Дней, через 5 лет вам будет ${data.Возраст5}`);
    rl.close;
}

{
    var data = {};
    const readline = require('readline');
    var rl = readline.createInterface({input: process.stdin, output: process.stdout});
    nameEnter(rl);
}



