'use strict'

function Person(name) {
    this._name = name;
    this._friends = [];
    this._spouse = null;
}

Person.prototype.sayHello = function () {
    var s = "hi, my name's " + this._name;
    if (this._friends.length) { // Имена друзей, если есть
        s = s + "\nMy friends:"
        for (var i = 0, n = this._friends.length; i < n; i++) {
            s = s + "\n" + this._friends[i]._name
        }
    }
    if (this._spouse) { // Имя супруга/и, если есть
        s = s + "\nСупруг(а): " + this._spouse._name;
    }
    s = s + "\n__________________________________________";
    console.log(s);
}


function Woman(name) {
    this.superConstructor.call(this, name);
}

var w = function () {
};
w.prototype = Person.prototype;
Woman.prototype = new w();
Woman.prototype.constructor = Woman;
Woman.prototype.super = w.prototype;
Woman.prototype.superConstructor = Person;


Woman.prototype.setFriend = function (person) {
    if (person instanceof Woman) {
        this._friends.push(person);
        person._friends.push(this);
    }
}

Woman.prototype.getMarry = function (person) {
    if (!this._spouse && person instanceof Man && !person._spouse) {
        this._spouse = person;
        person._spouse = this;
    }
}

function Man(name) {
    this.superConstructor.call(this, name);
}

function Woman(name) {
    this.superConstructor.call(this, name);
}

var m = function () {
};
m.prototype = Person.prototype;
Man.prototype = new m();
Man.prototype.constructor = Man;
Man.prototype.super = m.prototype;
Man.prototype.superConstructor = Person;


Man.prototype.setFriend = function (person) {
    if (person instanceof Man) {
        this._friends.push(person);
        person._friends.push(this);
    }
}

Man.prototype.getMarry = function (person) {
    if (!this._spouse && person instanceof Woman && !person._spouse) {
        this._spouse = person;
        person._spouse = this;
    }
}

var ivan = new Man("Иван"); // Иван - экземпляр мужчины
var peter = new Man("Петр"); // Петр - экземпляр мужчины
var and = new Man("Андрей");  // Андрей - экземпляр мужчины
var ann = new Woman("Анна");  // Анна - экземпляр женщины
var olga = new Woman("Ольга"); // Ольга - экземпляр женщины
ivan.setFriend(peter); // Петр - друг Ивана
ivan.setFriend(and); // Андрей - друг Ивана
ivan.getMarry(and); // Андрей - не может жениться на Иване
ivan.getMarry(ann); // Анна вышла замуж на Ивана
ivan.getMarry(olga); // Жениться можно только один раз
olga.getMarry(and);
ivan.sayHello();
ann.sayHello();
and.sayHello();
olga.sayHello();

