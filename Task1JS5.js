'use strict';

function dataTestString(name) {
    while (1) {
        var data = prompt(name);
        if (/^[а-яё, aA-zZ]+$/i.test(data)) {
            break;
        }
        alert("Ввод некорректен, попробуйте еще раз");
    }
    return data;
}

function dataTestNumber(name) {
    while (1) {
        var data = prompt(name);
        if ((/^[0-9]+$/i.test(data)) && (data > 0)) {
            break;
        }
        alert("Ввод некорректен, попробуйте еще раз");
    }
    return data;
}

{
    var firstName = "Введите имя";
    var middleName = "Введите ваше отчество";
    var lastName = "Введите вашу фамилию";
    var age = "Введите ваш возраст";
    var ageDays;
    var ageNext;
    var pension = "нет";
    var sex = "ваш пол - мужской?";
    firstName = dataTestString(firstName);
    middleName = dataTestString(middleName);
    lastName = dataTestString(lastName);
    age = +dataTestNumber(age);
    sex = confirm("ваш пол - мужской?");
    ageDays = age * 365;
    ageNext = (age + 5);
    if (age > 55) {
        pension = "да"
    }
    if (sex) {
        sex = "Мужской"
    } else {
        sex = "Женский"
    }
    alert("ваше ФИО: " + lastName + " " + firstName + " " + middleName + "\n"
        + "ваш возраст в годах: " + age + "\n" +
        "ваш возраст в днях: " + ageDays + "\n" +
        "через 5 лет вам будет: " + ageNext + "\n" +
        "ваш пол: " + sex + "\n" +
        "вы на пенсии: " + pension);
    document.body.innerHTML = "<p>ваше ФИО: " + lastName + " " + firstName + " " + middleName +
        "</p><p>ваш возраст в годах: " + age +
        "</p><p>ваш возраст в днях: " + ageDays +
        "</p><p>через 5 лет вам будет: " + ageNext +
        "</p><p>ваш пол: " + sex +
        "</p><p>вы на пенсии: " + pension + "</p>";
}

