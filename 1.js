'use strict';

function areArraysEqual(a1, a2) {
    if ((a1 instanceof Array) && (a2 instanceof Array )) {
        if (a1.length === a2.length) {
            for (var i = 0; i < a1.length; i++) {
                if (a1[i] !== a2[i]) {
                    return false;
                }
            }
        }
        return true;
    }
    else {
        return false;
    }
}

function randomArray(length, height) {
    var array = [];
    for (var i = 0; i < length; i++) {
        array[i] = [];
        for (var j = 0; j < height; j++) {
            array [i] [j] = Math.floor(Math.random() * (length - height + 1) * 100) / 10;
        }
    }
    return array;
}

function matrix(length, height) {
    var array = randomArray(length, height);
    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        console.log("[" + array[i].toString() + "]");
    }
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < i; j++) {
            sum = sum + array[i] [j];
            console.log(sum);
        }
    }
    sum = Math.floor(sum * 10) / 10;
    console.log("Сумма элементов ниже диагонали равна : " + sum);
}

function transparent(length, height) {
    var array = randomArray(length, height);
    var transparentArray = [];
    for (var i = 0; i < array.length; i++) {
        console.log("[" + array[i].toString() + "]");
    }
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array[i].length; j++) {
            transparentArray [j] = [];
            transparentArray [j][i] = array[i] [j];
            console.log("element " + j + " " + i + " = " + transparentArray[j][i]);
        }
    }
    for (var i = 0; i < transparentArray.length; i++) {
        console.log("[" + transparentArray[i].toString() + "]");
    }
}

{
    transparent(2, 5);
    matrix(5, 5);
    alert(areArraysEqual([1, 2, 3], [1, 4, 5]));
    alert(areArraysEqual([1, 2, 3], [1, 2, "3"]));
    alert(areArraysEqual([1, 2, 3], [1, 2, 3]));
    alert(areArraysEqual([], []));
    alert(areArraysEqual([], null));
}


(function () {
    var a = [];
    for (var i = 0; i < 10; i++) {
        a.push(function () {
            console.log(i);
        });
    }
    for (var j = 0; j < 10; j++) {
        a[j]();
    }
})();

(function () {
    var a = [];
    for (var i = 0; i < 10; i++) {
        a.push((function (x) {
            return function () {
                console.log(x);
            }
        })(i));
    }
    for (var j = 0; i < 10; j++) {
        a[j]();
    }
})();

