'use strict';

function dataTestString(name) {
    while (1) {
        data = prompt(name);
        if (/^[а-яё, aA-zZ]+$/i.test(data)) {
            break;
        }
        alert("Ввод некорректен, попробуйте еще раз");
    }
    return data;
}

function dataTestNumber(name) {
    while (1) {
        data = prompt(name);
        if ((/^[0-9]+$/i.test(data)) && (data > 0)) {
            break;
        }
        alert("Ввод некорректен, попробуйте еще раз");
    }
    return data;
}

{
    var data;
    let firstName = "Введите имя";
    let middleName = "Введите ваше отчество";
    let lastName = "Введите вашу фамилию";
    let age = "Введите ваш возраст";
    let ageDays;
    let ageNext;
    let pension;
    let sex = "ваш пол - мужской?";
    firstName = dataTestString(firstName);
    middleName = dataTestString(middleName);
    lastName = dataTestString(lastName);
    age = +dataTestNumber(age);
    sex = confirm("ваш пол - мужской?");
    ageDays = age * 365;
    ageNext = (age + 5);
    alert(`ваше ФИО:  ${lastName} ${firstName} ${middleName} \n ваш возраст в годах:  ${age} \n ваш возраст в днях:   ${ageDays} \n через 5 лет вам будет: ${ageNext} \n ваш пол:  ${sex ? "Мужской" : "Женский"} \n вы на пенсии: ${age >= 55 ? "Да" : "Нет"}`);
    document.body.innerHTML = `ваше ФИО:  ${lastName} ${firstName} ${middleName} <br>
     ваш возраст в годах:  ${age} <br>
     ваш возраст в днях:   ${ageDays} <br>
     через 5 лет вам будет: ${ageNext} <br>
     ваш пол:  ${sex ? "Мужской" : "Женский"} <br>
     вы на пенсии: ${age >= 55 ? "Да" : "Нет"}`;
}

