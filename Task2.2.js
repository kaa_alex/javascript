"use strict"

function randomArray(length) {
    var array = [];
    for (var i = 0; i < length; i++) {
        array[i] = Math.floor(Math.random() * 10);
    }
    return array;
}

function arrayChange(length) {
    let arrayHalf = Math.floor(length / 2);
    let sortStart = arrayHalf + (length % 2);
    let array = randomArray(length);
    console.log(array.toString());
    for (let i = 0; i < arrayHalf; i++) {
        let x = array[i];
        array[i] = array[sortStart + i];
        array[sortStart + i] = x;
    }
    console.log(array.toString());
}

console.log("Тест 1 : ")
arrayChange(6);
console.log("Тест 2 : ");
arrayChange(5);
console.log("Тест 3 : ");
arrayChange(9);