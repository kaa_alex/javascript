'use strict'

class HashStorage {
    constructor() {
        this.hash = {};
    }

    addValue(key, value) {
        this.hash[key] = value;
    }

    getValue(key) {
        return this.hash[key];
    }

    deleteValue(key) {
        if (this.getValue(key) != undefined) {
            delete this.hash[key];
            return true;
        } else {
            return false;
        }
    }

    getKeys() {
        return Object.keys(this.hash);
    }
}

const drinkStorage = new HashStorage();

function newDrink() {
    let reciep;
    let name = prompt("Введите название напитка");
    if (name != null && name != '') {
        if (confirm("Напиток алкогольный ?")) {
            reciep = 'да/';
        } else {
            reciep = 'нет/'
        }
        let reciepEnter = prompt("Введите рецепт :");
        if (reciepEnter != null && reciepEnter != '') {
            reciep = reciep + reciepEnter;
            drinkStorage.addValue(name, reciep);
        } else {
            alert('Ввод отменен или некорректен!');
        }
    } else {
        alert('Ввод отменен или некорректен!');
    }
}

function allDrinks() {
    let drinkList = '';
    let drinks = drinkStorage.getKeys();
    if (drinks.length > 0) {
        for (let i = 0; i < drinks.length; i++) {
            drinkList = drinkList + `${drinks[i]} <br>`;
        }
    } else {
        drinkList = "список пуст";
    }
    document.getElementById('info').innerHTML = drinkList;
}

function deleteDrink() {
    let name = prompt("Введите название напитка который хотите удалить:");
    if (name != null && name != '') {
        if (drinkStorage.deleteValue(name)) {
            alert('Напиток успешно удален');
        } else {
            alert('Такого напитка нету в базе');
        }
    } else {
        alert('Ввод отменен или некорректен!');
    }
}

function getInfo() {
    let infoList = '';
    let name = prompt("Введите название напитка:");
    if (name != null && name != '') {
        let info = drinkStorage.getValue(name);
        if (info != undefined) {
            let information = info.split('/');
            infoList = `Название:${name} <br> Алкогольный: ${information[0]} <br> Рецепт: ${information[1]} `;
        } else {
            alert("Такого напитка нет в базе");
        }
    } else {
        alert('Ввод отменен или некорректен!');
    }
    document.getElementById('info').innerHTML = infoList;
}
